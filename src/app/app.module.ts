import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmpireInputComponent } from './empire-input/empire-input.component';
import { SurvivabilityOutputComponent } from './survivability-output/survivability-output.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiModule, BASE_PATH } from '@marco/starwars_angular_client';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    EmpireInputComponent,
    SurvivabilityOutputComponent
  ],
  imports: [
    ApiModule,
    HttpClientModule,
    BrowserModule
  ],
  providers: [{ provide: BASE_PATH, useValue: environment.API_BASE_PATH }],
  bootstrap: [AppComponent]
})
export class AppModule { }
