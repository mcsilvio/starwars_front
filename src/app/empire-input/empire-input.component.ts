import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable, Subject } from "rxjs"

@Component({
  selector: 'app-empire-input',
  template: `
    <h3>Empire Activity</h3>
      <p>Please enter your Empire Activity file. The default configuration will be used to calculate the probability.</p>
      <div class="form-group">
      <label for="file" id="file-label">Choose File:</label>
      <input type="file" id="file" (change)="handleFileInput($event)">

      <div class="error-message" *ngIf="error">
         <p>{{error | async}}</p>
      </div>
    </div>
  `,
  styles: [
    '.error-message { color: red; }',
    '#file-label { margin-right: 50px; color: #ddddff;}'
  ]
})
export class EmpireInputComponent implements OnInit {

  @Output() fileContentsSubmitted = new EventEmitter();

  reader = new FileReader();
  error = new Subject<String>();


  handleFileInput($event) : void {
    var file:File = $event.target.files[0]; 
    this.reader.onload = (function(emitter, error){

      var empireFileIsValid = function(empireFileContents){
        var empire;
        try {
           empire = JSON.parse(empireFileContents);
        } catch(Error) {
           return false;
        }

        if ( empire.countdown === undefined || empire.bounty_hunters === undefined ) {
           return false;
        }

        return true; 
      };

      return function(e){
        var fileContents = e.target.result;
        if (empireFileIsValid(fileContents)){
          emitter.emit(fileContents);
          error.next(null);
        } else {
          emitter.emit(null);
          error.next("The given EmpireActivity file was invalid");
        }
      };
    })(this.fileContentsSubmitted, this.error);   
    this.reader.readAsText(file);
  }

  ngOnInit(): void { }
}
