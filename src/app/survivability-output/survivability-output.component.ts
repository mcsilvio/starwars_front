import { Observable } from "rxjs"
import { Component, Input, OnInit } from '@angular/core';
import { Survivability } from '@marco/starwars_angular_client';

@Component({
  selector: 'app-survivability-output',
  template: `
    <div *ngIf="survivability | async as surv">
      <h3>Probability of Survival</h3>
      <p> The probability of the Millenium Falcon surviving its journey is ...</p>
      <p class="percentage">%{{ surv.probability }}</p>
    </div>


  `,
  styles: ['.percentage{font-size: 30px;}']
})
export class SurvivabilityOutputComponent implements OnInit {

  @Input() survivability: Observable<Survivability>

  constructor() { }

  ngOnInit(): void {
  }

}
