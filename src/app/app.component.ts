import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DefaultService, EmpireActivity, Survivability } from '@marco/starwars_angular_client';

@Component({
  selector: 'app-root',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->
    <div style="text-align:center" class="content">
      <h1>
        Millenium Falcon Survivability Calculator
      </h1>
      <p>Calculate the probability of the Millenium Falcon surviving its journey!</p>
      <app-empire-input (fileContentsSubmitted)="calculateSurvivability($event)"></app-empire-input>
      <br />
      <app-survivability-output [survivability]="response"></app-survivability-output>
  `,
  styles: []
})
export class AppComponent {
  response: any;
  defaultService: DefaultService;

  constructor(defaultService: DefaultService){
    this.defaultService = defaultService;
  }

  calculateSurvivability(fileContents){
    if (fileContents) {
       this.response = this.defaultService.survivabilityDefaultConfigPost(fileContents);
    } else {
       this.response = null;
    }
  }
}
