# Star Wars Survivability Calculator - Front-End User Interface
This is an Angular website for use with the Star Wars Survivability Calculator API (https://bitbucket.org/mcsilvio/starwars_api).

## Requirements.

- node v12+
- npm
- npx

## Tech Stack Used

- node, npm, npx
- angular 9
- swagger generated client for Survivability Calculator API (https://bitbucket.org/mcsilvio/starwars_angular_client/)

## Installation & Usage

Perform the following steps to install and use the program.  

1. Clone the repository to a local folder
2. From that folder, install dependencies with 
`npm install`
3. Configure the base url of the Survivability Calculator API by setting the following value in the `src/environments/environments.ts` file.  
`API_BASE_PATH = 'http://localhost:8080'`
4. Start the server by running  
`npx ng serve`
5. Visit `http://localhost:4200/` in your browser.

## Usage

Once the server is started, and you visit the site in your browser, you will see a one-page application with the title Millenium Falcon Survivability Calculator. There is one input and that is a file input. Here you can supply a valid Empire Activity file. Once the file is uploaded, a request is made to the API. The API will use the given EmpireActivity file and a default MilleniumFalcon file to calculate the survivability of the Millenium Falcon on its journey. The probability of survival will be displayed below the file input. It is a percentage probability (e.g. 0% to 100%). You can upload another file to start this process all over again.


## Author

Silvio Marco Costantini


